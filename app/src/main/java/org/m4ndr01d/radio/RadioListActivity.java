package org.m4ndr01d.radio;
import android.app.*;
import android.content.*;
import android.graphics.drawable.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import org.m4ndr01d.radio.utils.*;
import android.content.Intent;


public class RadioListActivity extends ListActivity{
	int kategori;
	String[] catname;
	String[] catlink;

	@Override
	protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

		switch(kategori = getIntent().getIntExtra("kategori", 0)){
			case 0:
				catname = getResources().getStringArray(R.array.rockhit_names);
				catlink = getResources().getStringArray(R.array.rockhit_links);
				break;
			case 1:
				catname = getResources().getStringArray(R.array.pop_names);
				catlink = getResources().getStringArray(R.array.pop_links);
				break;
			case 2:
				catname = getResources().getStringArray(R.array.yabanci_names);
				catlink = getResources().getStringArray(R.array.yabanci_links);
				break;
			case 3:
				catname = getResources().getStringArray(R.array.rap_names);
				catlink = getResources().getStringArray(R.array.rap_links);
				break;
			case 4:
				catname = getResources().getStringArray(R.array.dini_names);
				catlink = getResources().getStringArray(R.array.dini_links);
				break;
			case 5:
				catname = getResources().getStringArray(R.array.haberspor_names);
				catlink = getResources().getStringArray(R.array.haberspor_links);
				break;
		}

		ArrayAdapter<String> aa = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,android.R.id.text1,catname);
		getListView().setAdapter(aa);
		getListView().setOnItemClickListener(new ListView.OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> p1,View p2,int p3,long p4){
					h.sendEmptyMessage(p3);
					
				}
			});
		
		startService(new Intent(this,PlayerService.class));
		bindService(new Intent(this,PlayerService.class),sc,BIND_EXTERNAL_SERVICE);
	}

	Handler h = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			try{
				ps.setMediaSource(catlink[msg.what],catname[msg.what]);
				ps.play();
			}catch(Throwable e){
				throw new RuntimeException(e);
			}
			super.handleMessage(msg);
		}

	};

	PlayerService ps = null;
	ServiceConnection sc = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName p1,IBinder p2){
			ps = ((PlayerService.ServiceBinder) p2).getService();
			ps.preparePlayer();
		}

		@Override
		public void onServiceDisconnected(ComponentName p1){
			ps = null;
		}

	};
}

