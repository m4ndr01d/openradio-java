package org.m4ndr01d.radio;

import java.lang.reflect.*;

public class ReflectHandler {
	private ReflectHandler(){}
	
	public static Object getFieldValue(Class<?> c, Object o, String field){
		try {
			Field f = c.getField(field);
			f.setAccessible(true);
			return f.get(o);
		} catch(Throwable e){}
		return null;
	}
	
	public static void setFieldValue(Class<?> c, Object o, String field, Object value){
		try {
			Field f = c.getField(field);
			f.setAccessible(true);
			f.set(o,value);
		} catch(Throwable e){}
	}
	
	public static Object processMethod(Object o, String method, boolean declared){
		return processMethod(o.getClass(),o,method,declared);
	}
	
	public static Object processMethod(Class<?> c, Object o, String method, boolean declared){
		return processMethod(c,o,method,null,null,declared);
	}
	
	public static Object processMethod(Object o, String method, Object[] args,  boolean declared){
		return processMethod(o.getClass(),o,method,args,declared);
	}
	
	public static Object processMethod(Object o, String method, Class<?>[] argClass, Object[] args,  boolean declared){
		return processMethod(o.getClass(),o,method,argClass,args,declared);
	}
	
	public static Object processMethod(Class<?> c, Object o, String method, Object[] args, boolean declared){
		Class<?>[] a = new Class<?>[args.length];
		for(int i = 0;i < args.length;i++){
			a[i] = args[i].getClass();
		}
		return processMethod(c,o,method,a,args,declared);
	}
	
	public static Object processMethod(Class<?> c, Object o, String method, Class<?>[] argClass, Object[] args, boolean declared){
		try {
			Method m = declared ? c.getDeclaredMethod(method,argClass) : c.getMethod(method,argClass);
			m.setAccessible(true);
			return m.invoke(o,args);
		} catch(Throwable e){}
		return null;
	}
	
	public static Object getConstructor(Class<?> c, boolean declared){
		return getConstructor(c,null,null,declared);
	}
	
	public static Object getConstructor(Class<?> c, Class<?>[] argClass, Object[] args, boolean declared){
		try {
			Constructor cs = declared ? c.getDeclaredConstructor(argClass) : c.getConstructor(argClass);
			cs.setAccessible(true);
			return cs.newInstance(args);
		} catch(Throwable e){}
		return null;
	}
}
