package org.m4ndr01d.radio;

import android.app.*;
import android.content.*;
import android.graphics.drawable.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import org.m4ndr01d.radio.utils.*;

public class MainActivity extends Activity {
	GridView gl = null;
	String[] cat = null;
	int[] catRes = {
		R.drawable.rock,
		R.drawable.pop,
		R.drawable.yabanci,
		R.drawable.rap,
		R.drawable.dini,
		R.drawable.haber
	};
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
		cat = getResources().getStringArray(R.array.categories);
		setContentView(v());
    }
	
	private View v(){
		gl = new GridView(this);
		gl.setLayoutParams(new GridView.LayoutParams(-1,-1));
		gl.setGravity(Gravity.CENTER_HORIZONTAL);
		gl.setNumColumns(2);
		int p = (int) Helper.mp(1);
		gl.setPadding(p,p,p,p);
		gl.setVerticalSpacing(p);
		gl.setHorizontalSpacing(p);
		gl.setOnItemClickListener(new GridView.OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> p1,View p2,int p3,long p4){
				//Toast.makeText(p1.getContext(),"Butona basıldı: "+p3,Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(MainActivity.this, RadioListActivity.class);
				intent.putExtra("kategori", p3);
				startActivity(intent);
			}
		});
		ArrayAdapter<String> la = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,android.R.id.text1,cat){
			int f = (int) Helper.dp(96);
			@Override
			public View getView(int p, View v, ViewGroup g){
				CardLayout cl = new CardLayout(MainActivity.this);
				cl.setLayoutParams(new ViewGroup.LayoutParams(-1,f));
				cl.setCard(getItem(p),catRes[p]);
				return cl;
			}
		};
		gl.setAdapter(la);
		return gl;
	}
	
	
}
