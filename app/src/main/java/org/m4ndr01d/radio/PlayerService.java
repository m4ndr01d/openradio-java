package org.m4ndr01d.radio;

import android.app.*;
import android.content.*;
import android.media.*;
import android.media.session.*;
import android.net.*;
import android.os.*;
import java.util.*;

public class PlayerService extends Service implements MediaPlayer.OnPreparedListener {

	@Override
	public void onPrepared(MediaPlayer p1){
		Helper.sendMediaNotification(this,mTitle,mSession);
		mPlayer.start();
	}
	
	private MediaPlayer mPlayer = new MediaPlayer();
	private final IntentFilter noisyFilter = getFilter();
	
	private static final IntentFilter getFilter(){
		IntentFilter f = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
		f.addAction(Helper.ACTION_PAUSE);
		return f;
	}
	
	public void adjustMediaSession(int state){
		if(Build.VERSION.SDK_INT >= 21){
			MediaSession ms = (MediaSession) mSession;
			PlaybackState ps = new PlaybackState.Builder().setState(state,0,1).build();
			ms.setPlaybackState(ps);
		}
	}
	
	public void setMediaSource(String url,String title) throws Throwable {
		mPlayer.reset();
		mPlayer.setDataSource(this,Uri.parse(url));
		mPlayer.setOnPreparedListener(this);
		if(Build.VERSION.SDK_INT >= 21){
			MediaSession ms = (MediaSession) mSession;
			ms.setMetadata(Helper.getMetadata(getResources(),mTitle = title));
		}
	}
	
	public void play() throws Throwable {
		registerReceiver(noisyRecv,noisyFilter);
		adjustMediaSession(PlaybackState.STATE_PLAYING);
		int result = audioManager.requestAudioFocus(listen,AudioManager.STREAM_MUSIC,AudioManager.AUDIOFOCUS_GAIN);
		if(result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
			if(mPlayer != null) mPlayer.prepareAsync();
		}
	}
	
	public void exit(){
		unregisterReceiver(noisyRecv);
		adjustMediaSession(PlaybackState.STATE_STOPPED);
		mPlayer.stop();
		stopForeground(true);
		audioManager.abandonAudioFocus(listen);
		stopSelf();
	}
	
	public boolean isPlaying(){
		return mPlayer.isPlaying();
	}
	
	private class NoisyReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context p1,Intent p2){
			exit();
		}
		
	}
	
	public void preparePlayer(){
		audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		listen = new AudioManager.OnAudioFocusChangeListener(){
			@Override
			public void onAudioFocusChange(int p1){
				switch(p1){
					case AudioManager.AUDIOFOCUS_GAIN:
						exit();
						break;
				}
			}
		};
		if(Build.VERSION.SDK_INT >= 21){
			try {
				final MediaSession session = new MediaSession(this,getClass().getSimpleName());
				mSession = session;
				session.setActive(true);
				MediaSession.Callback cb = new MediaSession.Callback(){
					@Override
					public void onPlay(){
						super.onPlay();
					}

					@Override
					public void onPause(){
						super.onPause();
						if(mPlayer.isPlaying()){
							exit();
						}
					}
				};
				session.setCallback(cb);
			} catch(Throwable e){}
		}
	}
	
	private String mTitle = null;
	private Object mSession = null;
	private AudioManager audioManager = null;
	private AudioManager.OnAudioFocusChangeListener listen = null;
	private IBinder binder = new ServiceBinder();
	private NoisyReceiver noisyRecv = new NoisyReceiver();

	@Override
	public IBinder onBind(Intent p1){
		return binder;
	}

	@Override
	public boolean onUnbind(Intent intent){
		return false;
	}
	
	public class ServiceBinder extends Binder {
		public PlayerService getService(){
            return PlayerService.this;
        }
    }
	
}
